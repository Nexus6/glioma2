# Copyright (c) 2015 Eric Anderson, https://www.linkedin.com/in/ericanderson
# Copyright (c) 2015 Teletap, LLC, or its affiliates.  All Rights Reserved
#
# This software is licensed for use under the terms of the GNU General
# Public License version 3.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABIL-
# ITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
# SHALL THE AUTHOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

"""
A suite of collection classes that emulate a small subset of those in the
Scala standard library.
"""
class Either(object):

    __slots__ = ['_content', '_is_left']

    def __init__(self, value, is_left):
        super(Either, self).__init__()
        self._content = value
        self._is_left = is_left

    def __eq__(self,other):
        return (self.__class__ == other.__class__) and self.get == other.get

    def __ne__(self,other):
        return not self.__eq__(other)

    def fold(self, do_left, do_right):
        return do_left(self._content) if self._is_left else do_right(self._content)

    @property
    def get(self):
        return self._content

    @property
    def is_left(self):
        return self._is_left

    @property
    def isLeft(self):
        return self._is_left

    @property
    def is_right(self):
        return not self._is_left

    @property
    def isRight(self):
        return not self._is_left

class Left(Either):

    def __init__(self, value=None):
        super(Left, self).__init__(value, True)

class Right(Either):

    def __init__(self, value=None):
        super(Right, self).__init__(value, False)
