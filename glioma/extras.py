# Copyright (c) 2015 Eric Anderson, https://www.linkedin.com/in/ericanderson
# Copyright (c) 2015 Teletap, LLC, or its affiliates.  All Rights Reserved
#
# This software is licensed for use under the terms of the GNU General
# Public License version 3.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABIL-
# ITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
# SHALL THE AUTHOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

"""
Extra Scala-inspired goodies.
"""
import functools
from .option import Option

class match(object):

    def __init__(self, this, parameters=None):
        self.__this = this
        self.__parameters = parameters
        self.__result = None

    def result(self, f):
        return f(self.__this, self.__parameters) if callable(f) else f

    def equals(self, that, f):
        if not self.__result and self.__this == that:
            self.__result = self.result(f)
        return self

    def isin(self, that, f):
        if not self.__result and self.__this in that:
            self.__result = self.result(f)
        return self

    def filter(self, test, f):
        if not self.__result and test(self.__this):
            self.__result = self.result(f)
        return self

    def classof(self, that, f):
        if not self.__result and isinstance(self.__this,that):
            self.__result = self.result(f)
        return self

    def default(self, f):
        if not self.__result:
            self.__result = self.result(f)
        return self

    def __call__(self): return Option(self.__result)

def compose(*functions):

    def _compose(f, g):
        return lambda x: f(g(x))

    return functools.reduce(_compose, functions, lambda x: x)