# Copyright (c) 2015 Eric Anderson, https://www.linkedin.com/in/ericanderson
# Copyright (c) 2015 Teletap, LLC, or its affiliates.  All Rights Reserved
#
# This software is licensed for use under the terms of the GNU General
# Public License version 3.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABIL-
# ITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
# SHALL THE AUTHOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

"""
A suite of collection classes that emulate a small subset of those in the
Scala standard library.
"""
class Optional(object):

    __slots__ = ['_content']

    def __init__(self, value):
        self._content = value

    def __iter__(self):
        return iter([self._content])

    def __eq__(self, other):
        return (self.__class__ == other.__class__) and self._content == other._content

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return self.toString

    def forall(self, test):
        return self.takeWhile(test).length == self.length

    def foreach(self, f):
        self.map(f)

    @property
    def get(self):
        return self._content

    def getOrElse(self, f):
        return self._content or (f() if callable(f) else f)

    def getOrNone(self):
        return self._content

    @property
    def isEmpty(self):
        return self._content is None

    def map(self, f):
        return Option(f(self._content))

    def __rshift__(self, f):
        return self.map(f)

    def orElse(self, f):
        rval = self if self._content is not None else f() if callable(f) else f
        if not isinstance(rval, Optional):
            raise TypeError("orElse() must return an Optional")
        return rval

    @property
    def orNone(self):
        return self

    @property
    def toList(self):
        from .collections import List
        return List(source=[self._content])

class Some(Optional):

    def __init__(self, value):
        super(Some, self).__init__(value)

    def filter(self, f):
        return self if f(self._content) else Nothing

    def mkString(self, sep=None):
        return str(self._content)

    def takeWhile(self, test):
        from .collections import List
        return List(self._content) if test(self._content) else List()

    @property
    def toString(self):
        return 'Some(%s)' % str(self._content)

class NoInstantiate(object):
    def __init__(self):
        super(NoInstantiate, self).__init__()
    def __new__(cls, *args, **kwargs):
        raise Exception("Cannot instantiate Nothing")

class NothingType(type, object):

    @property
    def get(self):
        raise TypeError("nothing comes of Nothing")

    @property
    def isEmpty(self):
        return True

    @property
    def orNone(self):
        return None

    @property
    def toList(self):
        from .collections import List           
        return List()

    @property
    def toString(self):         return "Nothing"

    def __repr__(self):         return self.toString
    def __iter__(self):         return [].__iter__()
    def __len__(self):          return 0
    def __rshift__(self, f):    return self.map(f)

    @staticmethod
    def orElse(f):
        rval = f() if callable(f) else f
        if not isinstance(rval, Optional):
            raise TypeError("orElse() function must return an Option")
        return rval

    @staticmethod
    def getOrNone():
        return None

    @staticmethod
    def getOrElse(f):
        return f() if callable(f) else f

    @staticmethod
    def filter(f):
        return Nothing

    @staticmethod
    def foreach(f):
        pass

    @staticmethod
    def map(f):
        return Nothing

    @staticmethod
    def mkString(sep=None):
        return ""

    @staticmethod
    def takeWhile(test):
        from .collections import List           
        return List()

Nothing = NothingType('Nothing',(NoInstantiate,),{})

def Option(value):
    return Some(value) if value is not None else Nothing


