# Copyright (c) 2015 Eric Anderson, https://www.linkedin.com/in/ericanderson
# Copyright (c) 2015 Teletap, LLC, or its affiliates.  All Rights Reserved
#
# This software is licensed for use under the terms of the GNU General
# Public License version 3.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABIL-
# ITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
# SHALL THE AUTHOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

"""
A suite of collection classes that emulate a small subset of those in the
Scala standard library.
"""
pass
