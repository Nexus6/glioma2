# Copyright (c) 2015 Eric Anderson, https://www.linkedin.com/in/ericanderson
# Copyright (c) 2015 Teletap, LLC, or its affiliates.  All Rights Reserved
#
# This software is licensed for use under the terms of the GNU General
# Public License version 3.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABIL-
# ITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
# SHALL THE AUTHOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

"""
A suite of collection classes that emulate a small subset of those in the
Scala standard library.
"""

import functools, itertools
from types import GeneratorType
from .option import Some, Nothing

def reify(f):
    """
    A decorator to call the BaseCollection_reify method before invoking the wrapped method.
    Reification is needed prior to calling methods that work on fully-realized collection elements
    rather than iterator or generator sources.  Example: BaseCollection.length()
    """
    @functools.wraps(f)
    def wrapped(self, *args, **kwargs):
        self._reify()
        return f(self, *args, **kwargs)
    return wrapped

class _frozendict(type({})):
    def __init__(self, *args, **kwargs):
        super(_frozendict, self).__init__(self, *args, **kwargs)
    def update(self, other): raise NotImplementedError
    def __setitem__(self, key, value): raise NotImplementedError

class BaseCollection(object):
    __slots__ = ['_content', '_source']

    def __init__(self, value):
        """
        Generic initializer for a collection instance.
        :param value: native (python) collection or None
        :type value: object
        :return: None
        :rtype: None
        """
        super(BaseCollection, self).__init__()
        assert value is not None
        self._source, self._content = value, None
        if not isinstance(value, GeneratorType):
            self._reify()

    def _reify(self):
        raise NotImplementedError

    @reify
    def __call__(self,index):
        """Returns the item at the specified index in the collection.
        :param index: index in collection to fetch from
        :type index
        :return: The object at the specified index
        :rtype: object
        """
        if index > (self.length - 1):
            raise IndexError("index %i out of bounds" % index)
        else:
            return self._content[index]

    def __contains__(self, what):
        """ Support Pythonic 'x in collection' expressions """
        return self.contains(what)

    @reify
    def __eq__(self,other):
        return type(self) is type(other) and other._equals(self)

    @reify
    def __hash__(self):
        return self._content.__hash__()

    @reify
    def __iter__(self):
        return iter(self._content.items() if isinstance(self._content,dict) else \
                [] if self._content is None or len(self._content) == 0 else
                self._content)


    def __rshift__(self, f):
        return self.map(f)

    @reify
    def _equals(self, other):
        return other._content == self._content

    @reify
    def contains(self, other):
        return other in self._content

    @reify
    def count(self, f):
        return 0 if self.length == 0 else functools.reduce(lambda acc, x: acc + 1 if f(x) else acc, self, 0)

    def filter(self, f):
        raise NotImplementedError
    def __ge__(self, f): return self.filter(f)

    def __and__(self, other): return self.filter(other)

    @reify
    def find(self, f):
        for _ in self:
            if f(_): return Some(_)
        return Nothing

    @reify
    def fold(self, init, f):
        if self.isEmpty:
            raise TypeError("fold() called on empty container")
        return functools.reduce(f, self, init)

    @reify
    def forall(self, test):
        return len(list(itertools.takewhile(test, self))) == self.length

    @reify
    def foreach(self, f):
        for _ in self: f(_)

    @property
    @reify
    def isEmpty(self):
        return self._content is None or len(self._content) == 0

    @property
    @reify
    def length(self):
        return len(self._content)

    def map(self,f):
        raise NotImplementedError

    def mkString(self, separator=""):
        return separator.join(str(_) for _ in self._content) if self._content is not None else\
            "source=" + str(self._source)

    @reify
    def reduce(self, f):
        if self.isEmpty:
            raise TypeError("reduce() called on empty container")
        return functools.reduce(f, self._content)

    @property
    def size(self):
        return self.length

    def takeWhile(self, test):
        raise NotImplementedError

    def _zip(self, cls, other):
        from itertools import izip
        return cls(source=izip(self, other))

    def _zipWithIndex(self, cls):
        if isinstance(self._content, dict):
            return cls(source=zip(self._content.items(), range(0, self.length)))
        return cls(source=zip(self._content, range(0, self.length)))

    def _tail(self, cls):
        if self.isEmpty:
            raise IndexError("tail() called on empty collection")
        return cls(source=list(self._content)[1:])

    def _head(self, cls):
        if self.isEmpty:
            raise IndexError("head() called on empty collection")
        return list(self._content)[0]

class List(BaseCollection):

    def __init__(self, *args, **kwargs):
        super(List, self).__init__(kwargs['source'] if 'source' in kwargs else args)

    @reify
    def __add__(self, other):
        other._reify()
        return List(source=self._content + other._content)

    def _reify(self):
        if self._content is None:
            self._content = tuple(self._source)
            self._source = None

    def __repr__(self):
        return self.toString

    def filter(self, f):
        src = self._content if self._content is not None else self._source
        return List(source=(_ for _ in src if f(_)))

    def flatMap(self, f):
        return self.map(f).flatten

    @property
    @reify
    def flatten(self):
        return List(source=itertools.chain.from_iterable(
            self._content
        ))

    @property
    @reify
    def head(self): return self._head(List)

    @reify
    def indexOf(self, what, index=0):
        return self.indexWhere(lambda _: _ == what, index)

    @reify
    def indexWhere(self, test, index=0):
        if index < 0 or index >= len(self._content):
            return -1
        else:
            for (i, x) in enumerate(self._content[index:]):
                if test(x) is True:
                    return i + index
        return -1

    @property
    @reify
    def last(self):
        if self.length == 0:
            raise IndexError("last() called on empty List")
        return self._content[-1]

    @property
    @reify
    def list(self):
        return list(self._content)

    def map(self, f):
        return List(source=(f(_) for _ in (self._content or self._source)))

    @property
    @reify
    def reverse(self):
        return List(source=list(self._content).reverse())

    @property
    @reify
    def sorted(self):
        return List(source=sorted(self._content))

    @reify
    def sortedWith(self, key):
        return List(source=sorted(self._content, key=key))

    @property
    @reify
    def sum(self):
        return sum(self._content)

    @property
    @reify
    def tail(self):
        return self._tail(List)

    @reify
    def take(self,count):     
        return  List() if count <= 0 or self.length == 0 else \
                List(source=self._content) if count >= self.length else \
                List(source=self._content[:count])

    @reify
    def takeRight(self, count):
        return  List() if count < 1 or self.length == 0 else \
                List(source=self._content) if count >= self.length else \
                List(source=self._content[count:])

    @reify
    def takeWhile(self, test):
        return List() if self.length == 0 else \
            List(source=itertools.takewhile(test, self._content))

    @property
    def toList(self):
        return List(source=self.list)

    @property
    def toSet(self):
        return Set(source=self.list)

    @property
    def toString(self):
        return 'List(%s)' % self.mkString(", ")

    @reify
    def zip(self,other):
        return self._zip(List, other)

    @reify
    def zipWithIndex(self):
        return self._zipWithIndex(List)

class Map(BaseCollection):

    def __init__(self, *args, **kvargs):
        super(Map, self).__init__(
            kvargs['source'] if 'source' in kvargs else args
        )

    def __repr__(self):
        return self.toString

    @reify
    def __call__(self, key):
        return self._content[key]

    def _reify(self):
        if self._content is None:
            self._content = dict(self._source)
            self._source = None

    @reify
    def contains(self, key):
        return key in self._content

    @reify
    def count(self, f):
        return functools.reduce(lambda acc, (k, v): acc + 1 if f((k, v)) else acc, self._content.items(), 0)

    def filter(self, f):
        if self._content is not None:
            return Map(source=dict(kv for kv in self._content.items() if f(kv)))
        else:
            return Map(source=(kv for kv in self._source if f(kv)))

    @reify
    def find(self, f):
        for item in self._content.items():
            if f(item):
                return Some(item)
        return Nothing

    @reify
    def forall(self, test):
        for (k, v) in self._content.items():
            if not test((k, v)): return False
        return True

    @reify
    def get(self, index):
        return Nothing if not index in self._content else Some(self._content[index])

    @reify
    def getOrElse(self, key, default):
        return self._content[key] if key in self._content else default

    @property
    @reify
    def head(self):
        keys = self._content.keys()
        if not keys:
            raise IndexError("head() called on empty Map")
        return (keys[0], self._content[keys[0]])

    @reify
    def isDefinedAt(self, key):
        return self.contains(key)

    @property
    @reify
    def last(self):
        keys = list(self._content.keys())
        if not keys:
            raise IndexError("last() called on empty Map")
        return (keys[-1], self._content[keys[-1]])

    def map(self, f):
        if self._content is not None:
            return Map(source=(f(kv) for kv in self._content.items()))
        else:
            return Map(source=(f((kv)) for kv in self._source))

    def mkString(self, separator=""):
        return separator.join((str(_) for _ in self._content.items())) if self._content is not None else str(self._source)

    @reify
    def takeWhile(self, test):
        return Map(source=(kv for kv in self._content.items() if test(kv)))

    @property
    @reify
    def dict(self):
        return self._content

    @property
    @reify
    def toList(self):
        return List(source=tuple(self._content.items()))

    @property
    @reify
    def toSet(self):
        return Set(source=self._content.items())

    @property
    def toString(self):
        return 'Map(%s)' % (self.mkString(", "))

    def zip(self, other):
        return self._zip(Map, other)

    @property
    def zipWithIndex(self):
        return self._zipWithIndex(Map)

class Set(BaseCollection):

    def __init__(self, *args, **kwargs):
        super(Set, self).__init__(kwargs['source'] if 'source' in kwargs else set(args))

    @reify
    def __call__(self,index):   return index in self._content

    def __repr__(self):
        return self.toString

    def _reify(self):
        if self._content is None:
            self._content = frozenset(self._source)
            self._source = None

    @property
    @reify
    def content(self):
        return self._content

    def filter(self, f):
        src = self._content if self._content is not None else self._source
        return Set(source=(_ for _ in src if f(_)))

    @reify
    def flatMap(self, f):
        return self.map(f).flatten

    @property
    @reify
    def flatten(self):
        return Set(source=itertools.chain.from_iterable(self._content))

    @property
    @reify
    def head(self):
        return self._head(Set)

    @reify
    def intersect(self, other):
        return Set(source=self._content.intersection(other.content))

    @property
    @reify
    def last(self):
        if self.length == 0:
            raise IndexError("last() called on empty Set")
        return list(self._content)[-1]

    def map(self, f):
        return Set(source=(f(_) for _ in (self._content or self._source)))

    @property
    @reify
    def set(self):
        return set(self._content)

    @property
    @reify
    def sum(self):
        return sum(self._content)

    @property
    @reify
    def tail(self):
        return self._tail(Set)

    @reify
    def takeWhile(self, test):
        return Set(source=itertools.takewhile(test, self._content))

    @property
    @reify
    def toList(self):
        return List(source=self._content)

    @property
    def toString(self):
        return 'Set(%s)' % self.mkString(", ")

    @reify
    def union(self, other):
        return Set(source=self._content.union(other.set))

    @reify
    def zip(self, other):
        return self._zip(Set, other)

    @property
    @reify
    def zipWithIndex(self):
        return self._zipWithIndex(Set)


